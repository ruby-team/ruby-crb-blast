ruby-crb-blast (0.6.9-5) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Aquila Macedo ]
  * d/control: bumped Standards-Version to 4.6.2.
  * d/watch: updated watch file reference link.
  * d/control: replaced 'ruby-interpreter' to '${ruby:Depends}'.

 -- Aquila Macedo Costa <aquilamacedo@riseup.net>  Thu, 13 Jul 2023 20:24:04 -0300

ruby-crb-blast (0.6.9-4) unstable; urgency=medium

  * Team upload
  * Add patch to drop git in gemspec (Closes: #954536)
  * Add patch to relax ruby-bio
  * Refresh d/patches
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no

 -- Utkarsh Gupta <utkarsh@debian.org>  Mon, 06 Apr 2020 02:35:55 +0530

ruby-crb-blast (0.6.9-3) unstable; urgency=medium

  [ Michael R. Crusoe ]
  * Update Vcs-* and Standards-Version
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Trim trailing whitespace.
  * Set upstream metadata fields: Name.

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Tue, 24 Sep 2019 14:17:21 +0200

ruby-crb-blast (0.6.9-2) unstable; urgency=medium

  * Run tests with actual number of cores, not 6. (Closes: 892066)

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Fri, 16 Mar 2018 15:09:31 -0700

ruby-crb-blast (0.6.9-1) unstable; urgency=medium

  * New upstream release. Tests pass again. (Closes: #890691)

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Fri, 02 Mar 2018 08:03:29 -0800

ruby-crb-blast (0.6.8-1) unstable; urgency=medium

  * New upstream release.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Fri, 02 Dec 2016 08:05:22 -0800

ruby-crb-blast (0.6.6-1) unstable; urgency=medium

  * New upstream release
  * Update to latest standards version
  * Finish removal of bindeps dependency (Closes: #830080), enabled the check
    of dependencies during build to prevent its reoccurance.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 09 Oct 2016 09:37:41 -0700

ruby-crb-blast (0.6.4-2) unstable; urgency=medium

  * Update to latest standards version
  * Fix Vcs-Git URL
  * drop ${shlibs:Depends}

 -- Michael R. Crusoe <crusoe@ucdavis.edu>  Sat, 05 Mar 2016 10:51:57 -0800

ruby-crb-blast (0.6.4-1) unstable; urgency=medium

  * Initial release (Closes: #800737)

 -- Michael R. Crusoe <crusoe@ucdavis.edu>  Sat, 19 Sep 2015 21:56:00 -0700
